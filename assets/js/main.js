;$(function() {


// Gathering the right data to work code
var window_width  = $(window).width();
var window_height = $(window).height();



/* setHeightFirstArticle() - Set the height of a burning article */

function setHeightFirstArticle() { 
  var window_height     = ($(window).height()) - 55; 
  var leftc_height      = $('.first__shock_js').height(); 
  var rightc_height     = $('.first__additional-js').height();  

  var colums_height = Math.max(leftc_height, rightc_height, window_height)
  
  if (window_height < leftc_height) {
      colums_height = window_height;
  }
  
  if ($(window).width() < 767) {
    // colums_height -= 10;
  }

  $('.first__shock_js').css('min-height', colums_height+'px')
  $('.first__additional-js').css('min-height', colums_height+'px')
  // console.log(leftc_height)
  
}

 
setHeightFirstArticle();

$(window).resize(function() { 
  setHeightFirstArticle();
});
 




/*********  mnt slider CODE START  ************/ 
if ($('.mnt_js').length > 0) {
  $('.mnt_js').owlCarousel({ 
    nav:        false,
    margin:     0,
    items:      6,
    loop:       true,
    dots:       false,
    mouseDrag:  true,
    touchDrag:  true, 
    autoWidth:true,
  }); 
}
/*********  mnt slider CODE END  ************/ 



/*********  minislider slider CODE START  ************/ 
if ($('.minislider').length > 0) {
    $('.minislider').owlCarousel({ 
      nav:        true,
      margin:     0,
      items:      1,
      loop:       true,
      dots:       false,
      mouseDrag:  true,
      touchDrag:  true,  
      navSpeed: 300,
      navText: ['',''], 
      onInitialized : function(event) {
        var slider =  $(event.target);
        slider.find('.owl-nav div').wrapAll("<div class='owl-panel'></div>");
        slider.find('.owl-nav .owl-panel div').wrapAll("<div class='owl-buttons'></div>");
        
        var panel = slider.find('.owl-nav .owl-panel');
        panel.prepend("<div class='owl-description'><div class='owl-title'></div><div class='owl-resource'></div></div>");
        
        var title = slider.find('.owl-item.active .minislider__title').html();
        panel.find('.owl-title').html(title);

        var resource = slider.find('.owl-item.active .minislider__resource').html();
        panel.find('.owl-resource').html(resource);
 
      },
      onTranslate : function (event) {
        var slider = $(event.target);
        var panel  = $(slider).find('.owl-panel');
        panel.find('.owl-title').fadeOut(100);
        panel.find('.owl-resource').fadeOut(100);
      },
      onTranslated : function (event) {
        var slider = $(event.target);
        var panel  = $(slider).find('.owl-panel');

        var title = slider.find('.owl-item.active .minislider__title').html();
        panel.find('.owl-title').html(title);

        var resource = slider.find('.owl-item.active .minislider__resource').html();
        panel.find('.owl-resource').html(resource);

        panel.find('.owl-title').fadeIn(100);
        panel.find('.owl-resource').fadeIn(100);
      },

    }); 
}
/*********  minislider slider CODE END  ************/ 


/*********  videoplay CODE START  ************/ 
$('.videoplay').click(function(event) {
  $(this).addClass('play');
});
/*********  videoplay CODE END  ************/ 



/*********  notegroup CODE START  ************/ 
if ($('.notegroup').length > 0) {
  $('.notegroup').css('position','absolute');
}
/*********  notegroup CODE END  ************/ 


/*********  notes CODE START  ************/ 
/*
Код для заметок в аналитической статье.
Цель: Позовляет заметкам позиционироваться относительно нужного коментария
ради которого они были созданны. (Замысел дизайнера)
Принцип работы:
1. В тексте у жирного коментария должен быть атрибут с id заметки который к нему привязан
data-note="note1"

2. В заметке нужно указать соотвествующий id
id="note1"

Скриншот с примером:
http://c2n.me/3M6uQB1


*/ 
if ($('.important[data-note]').length > 0) {

  function setPositionInNotes() {
    $('.important[data-note]').each(function(index, el) {
      // var top = $(el).offset().top()
      var offset = $(el).offset()
      var top = offset.top
      
      var barriers = $('.pagecontent-js').offset().top;
      var id = "#" + $(el).data('note');
      $(id).css('top', top-barriers);
    });
  }

  setTimeout(function() {
    setPositionInNotes();
    $('.notegroup').addClass('show');
  }, 2000)
  
  
  $(window).resize(function(event) {
    setPositionInNotes();
  });


}
/*********  notes CODE START  ************/ 


/*********  MENU CODE START  ************/ 
$('.burger').click(function(event) {
  $('.menu').addClass('show'); 
  return false;
});

$('.menu__close').click(function(event) {
  $('.menu').removeClass('show'); 
  return false;
});

$('.menu_substrate').click(function(event) {
  $('.menu').removeClass('show'); 
});

/*********  MENU CODE END  ************/ 


/*********  COMMENTS CODE START  ************/ 
$('.social__comments-js').click(function(event) {
  $('.comments').addClass('show');
  $('body').css('overflow', 'hidden');
  return false;
});

$('.comments__close').click(function(event) {
  $('.comments').removeClass('show');
  $('body').css('overflow', 'auto');
  return false;
});

$('.comments_substrate').click(function(event) {
  $('body').css('overflow', 'auto');
  $('.comments').removeClass('show');
});
/*********  COMMENTS CODE END  ************/ 



/*********  TEXTAREA IN COMMENTS CODE START  ************/ 
$('.comments__textarea-js').focus(function(event) {
  $(this).addClass('focus');
});
$('.comments__textarea-js').focusout(function(event) {
  $(this).removeClass('focus');
})

/*********  TEXTAREA IN COMMENTS CODE END  ************/ 







/*********  MOBILE CODE START  ************/ 
  
//Show social blocks in header for intertal page
    $(window).scroll(function(event) {
      if ($(this).scrollTop() > 1 ) {
        $('.header_need_scroll-js').addClass('header_scroll');
      }
      else {
        $('.header_need_scroll-js').removeClass('header_scroll');
      } 
    });

//Add a background for website headers in mobile version
    $(window).scroll(function(event) {

      var horizon = 1;

      if ($('.advert').length > 0 && $('.advert').is(':visible')) {
        horizon = $('.advert').height();
      }

      if ($(this).scrollTop() > horizon ) {
        $('.header_need_trasparent-js').removeClass('header_transparent');
        
        if ($('.advert').length > 0 && $('.advert').is(':visible')) {
          $('.header').css('position','fixed');
        }
        
      }
      else {
        $('.header_need_trasparent-js').addClass('header_transparent');
        if ($('.advert').length > 0 && $('.advert').is(':visible')) {
          $('.header').css('position','relative');
        }
        
      } 

    });



//Show burger in header
    $(window).scroll(function(event) {
      if ($(this).scrollTop() > 1 ) {
        $('.header_need_burger-js').addClass('header_show_burger');
      }
      else {
        $('.header_need_burger-js').removeClass('header_show_burger');
      } 
    });


/*********  MOBILE CODE END ************/ 




/*********  POPUPS CODE END ************/ 

  // Logic drop down Windows start

  //Show popup
  $('a[data-popup]').click(function(event) {
    
    var selector = $(this).data('popup');
    
    $('.popup__back-js').fadeIn('400');
    $('[role='+selector+']').fadeIn('400');
    $('body').css('overflow','hidden');

    return false; 
  });
 

  $('.popup__close').click(function(event) {

    var thepopup = $(this).parents('.popup__box');

    $('.popup__back-js').fadeOut('400');
    thepopup.fadeOut('400');
    $('body').css('overflow','auto');
    
    return false;
  });

  $('.popup__back-js').click(function(event) {

    var thepopup = $(this).parents('.popup__box');

    $('.popup__back-js').fadeOut('400');
    thepopup.fadeOut('400');
    $('body').css('overflow','auto');
    
    return false;
  });

  $('.popup__table-js').click(function(event) {
 
    if ($(event.target).is('.popup__tr-js')) {
      event.stopPropagation()
      var thepopup = $(this).parents('.popup__box').addClass('fadeOutUp');

      $('.popup__back-js').fadeOut('400');
      thepopup.fadeOut('400');
         
      $('body').css('overflow','auto');
    }
    
  });

  // Logic drop down Windows end

/*********  POPUPS CODE END ************/ 


/*********  INPUT UI PLACEHOLDER CODE START ************/ 
  $('.inputlabel__field').focusout(function(){
    if ($(this).val() !== '') {
      $(this).addClass('not_empty');
    }
    else {
      $(this).removeClass('not_empty');
    }
  });  


  $('.inputlabel__placeholder').click(function(event) {
    $(this).prev('input').focus();
    $(this).prev('.select2-container').focus();
    
  });

/*********  INPUT UI PLACEHOLDER CODE END ************/ 



/*********  DATAPICKER START ************/ 
    $('#datepicker').click(function(event) { 
      
      $('.dataselect').addClass('show').fadeIn(300);

      
      setPositionDataPicker($(this));

      return false;
    });

    $('.dataselect__back').click(function(event) {
      $('.dataselect').removeClass('show').fadeOut(300);
    });


   $('.dataselect__datapicker').datepicker({ 
      buttonText:      "Select date",
      monthNames:      ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ],
      dayNames:        ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
      dayNamesMin:     [ "Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" ],
      showButtonPanel: true,
      firstDay:        1,
      currentText:     "Сегодня",
      closeText:       "Применить",
      onSelect: function(d, ob) { 
        var dataArray = d.split('/');
        var dataDate  = $(this).datepicker('getDate');

        var days   = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота","Воскресенье"];
        var months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь" ];

        var month     = parseInt(dataArray[0]);
        var number    = parseInt(dataArray[1]);
        var year      = parseInt(dataArray[2]);
        var dayOfWeek = dataDate.getUTCDay();

        month     = months[month-1];
        dayOfWeek = days[dayOfWeek];

        $('.dataselect__day').html(dayOfWeek);
        $('.dataselect__month').html(month);
        $('.dataselect__number').html(number);
        $('.dataselect__year').html(year);
  
        
      }
    });
/*********  DATAPICKER END ************/ 


/*********  CLOSE ADVERT END ************/ 

$('.advert__close').click(function(event) {
  closeAdvert();
  return false;
});
/*********  CLOSE ADVERT END ************/ 
  



  $(window).resize(function(event) {

    setMaxHeightBand();
    setMaxHeightFullCards();
    if ($('#datepicker').length > 0 ) {
      setPositionDataPicker($('#datepicker'));
    }

  });

});



/******** HELP FUNCTIONS START********/ 

function getMaxOfArray(numArray) {
  return Math.max.apply(null, numArray);
} 

function setMaxHeightBand() {
  $('.band_set_height-js').each(function(index, el) {

    var needHeight = 0;
    var ind = $(el).parent().parent().index(); 
    if (ind === 0) {
      needHeight = $(el).parent().parent().next().height()
    }
    else {
      needHeight = $(el).parent().parent().prev().height()
    }
    needHeight = needHeight - 24;

    $(el).css('max-height', needHeight);
    
  });
}

function setMaxHeightFullCards() {
  $('.card.card_full').each(function(index, el) {
    
    var row       = $(el).closest('.row').width();
    var parent    = $(el).closest('[class*="col-"]');
    var siblings  = parent.siblings('[class*="col-"]');
    var elHeight  = parent.height();


    var wiSiblings = siblings.css('width')  
    
    if (siblings.length > 0) {
      var siblingsHeight = [];
      siblings.each(function(index, ele) {
        var siblingH = $(ele).height();
        siblingsHeight.push(siblingH)
      });      

      var maxHeight = getMaxOfArray(siblingsHeight);
       

      if ((elHeight < maxHeight || elHeight > maxHeight) && parent.width() !== (row-32)) {
        maxHeight -= 24;
        $(el).find('.card__photo').css('min-height', maxHeight+'px');
      }
      if (parent.width() === (row-32)) {
         $(el).find('.card__photo').css('min-height', 'auto');
      }
    } 
  });
 
}


function setPositionDataPicker(el) {
  var thisPosition = $(el).offset();
  var thisWidth    = $(el).innerWidth();
  var thisHeight   = $(el).innerHeight();
  var leftPosition = thisPosition.left; 
  leftPosition -= 128;
  leftPosition += (thisWidth/2);
  $('.dataselect').css('left', leftPosition+'px' )
}

function closeAdvert() {
  $('.advert').slideUp(400, function() {
    $(this).remove();
    $('.header').css('position', 'fixed');
  });
}
 

/******** HELP FUNCTIONS END********/ 

 

/**** ONLOAD EVENTS ****/ 
window.onload = function() {
    setMaxHeightBand();
    setMaxHeightFullCards();
};
/**** ONLOAD EVENTS ****/ 


