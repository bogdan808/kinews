;
var gulp        = require('gulp'); 
var connect     = require('gulp-connect');  
var swig        = require('gulp-swig'); 
var notify      = require('gulp-notify'); 
var concat      = require('gulp-concat'); 
var fs 	        = require('fs-extra') ;
var args        = require('yargs').argv;
var spritesmith = require('gulp.spritesmith');
var buffer      = require('vinyl-buffer');
var csso        = require('gulp-csso');
var imagemin    = require('gulp-imagemin');
var merge       = require('merge-stream');
var os          = require('os'); 
var open        = require('gulp-open');


var option = {
    port: '9033'
}

var cssFiles = [  
    "assets/css/normalize.css",
    "assets/css/bootstrap.css",
    "assets/css/bootstrap-extension.css",
    "assets/css/jquery-ui.css",
    "assets/css/owl.carousel.css",
    "assets/css/owl.theme.default.css",
    "assets/css/owl.theme.mini.css",
    "assets/css/main.css",
    "assets/css/ui.css",
    "assets/css/grid.css",
    "assets/css/mainnavigation.block.css",
    "assets/css/subscription.block.css",
    "assets/css/burger.block.css",
    "assets/css/header.block.css",
    "assets/css/related.block.css",
    "assets/css/more.block.css",
    "assets/css/social.block.css",
    "assets/css/pagecontent.block.css",
    "assets/css/minislider.block.css",
    "assets/css/videoplay.block.css",
    "assets/css/note.block.css",
    "assets/css/sign.block.css",
    "assets/css/mainnews.block.css",
    "assets/css/survey.block.css",
    "assets/css/figure.block.css",
    "assets/css/autor.block.css",
    "assets/css/autors.block.css",
    "assets/css/menu.block.css",
    "assets/css/comments.block.css",
    "assets/css/popup.block.css",
    "assets/css/search.block.css",
    "assets/css/subscribe.block.css",
    "assets/css/found.block.css",
    "assets/css/collectioneditor.block.css",
    "assets/css/first.block.css",
    "assets/css/roubric.block.css",
    "assets/css/analysis.block.css",
    "assets/css/clipping.block.css",
    "assets/css/cards.block.css",
    "assets/css/band.block.css",
    "assets/css/mnt.block.css",
    "assets/css/dataselect.block.css",
    "assets/css/footer.block.css",
    "assets/css/form.css",
    "assets/css/print.css",
    "assets/css/animate.css",
    "assets/css/icons.css",
    ];


// swig == http://paularmstrong.github.io/swig/
gulp.task('swig', function() {
  // gulp.src(['./p/rubrick_2.html'])
  gulp.src('./p/*.html') 
    .pipe(swig({
      defaults: { cache: false }
    }))
    .pipe(gulp.dest('./')) 
    .pipe(connect.reload())
    .pipe(notify('SWIG compiled!'));
});



// server connect
gulp.task('connect', function () {
  connect.server({
    root: './',
    port: option.port,
    livereload: true
  });
}); 


 
  
// watch
gulp.task('watch', function () {  
  gulp.watch('./p/*.html', ['swig']);
  gulp.watch('./b/*.html', ['swig']);  
})

// default
gulp.task('default', [ 'connect', 'swig',  'watch', 'open', 'navigation']);
 

// experimentation start 
 


// СОЗДАНИЕ НОВОГО БЛОКА
gulp.task('b', function() {
    
    if (!args.env)
        throw console.log('Ошибка: Не установленно имя блока')

    var name = args.env;

    fs.ensureFile('b/'+name+'.block.html', function(){});

    fs.ensureFile('assets/css/'+name+'.block.css', function(){});

});
 
 

// ======== Таски для сборки проекта

gulp.task('dist', ['buildHtml', 'buildImages','buildMedia','buildJs', 'concatCss', 'buildFonts']);


gulp.task('buildHtml', function() {

	gulp.src('./*.html')
	.pipe(gulp.dest('~dist')); 

});


gulp.task('buildImages', function() {
	
	gulp.src('./assets/images/**/*.*')
	.pipe(gulp.dest('~dist/assets/images'));

});


gulp.task('buildMedia', function() {
	
	gulp.src('./assets/media/**/*.*')
	.pipe(gulp.dest('~dist/assets/media'));

});


gulp.task('buildJs', function() {
  
  gulp.src('./assets/js/**/*.*')
  .pipe(gulp.dest('~dist/assets/js'));

});


gulp.task('buildFonts', function() {
	
	gulp.src('./assets/fonts/**/*.*')
	.pipe(gulp.dest('~dist/assets/fonts'));

});


gulp.task('concatCss', function() {

  gulp.src(cssFiles)
    .pipe(concat('styles.css')) 
    .pipe(gulp.dest('~dist/assets/css'));

});



// Создание спрайтов
gulp.task('icons', function() {

 var spriteData = gulp.src('./assets/images/icons/*.png').pipe(
  spritesmith({
    imgName: 'sprite.png',
    cssName: 'icons.css', //поменять на icons.css,
    imgPath: '../images/sprite.png', 
  }));
 
  var imgStream = spriteData.img 
    .pipe(buffer())
    .pipe(imagemin())
    .pipe(gulp.dest('./assets/images'));


  var cssStream = spriteData.css 
    .pipe(gulp.dest('./assets/css'));

    return merge(imgStream, cssStream);

});

 
// Открыть страницу текущего проекта
gulp.task('open', function(){
  
  var uri = 'http://localhost:' + option.port
  
  console.log(uri);
  gulp.src('')
  .pipe(open(
    {
        app: 'chrome',
        uri: uri
    }
    ));
});

function log(string) {
    console.log(string);
}

//Создаем файл навигации по готовым страницам проекта
gulp.task('navigation', function() {
    
    

    //Компилируем навигацию по страницам
    fs.readdir('./p', function(err, files){ 

    var html = '<div class="pagenavigation">\n'

       for (var i = 0; i < files.length; i++) {
           var href = 'href="/' + files[i] + '"' ;
           var a    = '<a '+ href +'>' + files[i] +'</a>\n';
           html += a;
       }
    
    html += '</div>'

    fs.outputFile('./help/nav_pages.html', html, function (err) {
        if (err) console.log(err);
    })
            
    });
    

    //Компилируем навигацию по иконкам
    fs.readdir('./assets/images/icons', function(err, files){ 
    var html = '<div class="iconsnavigation">\n'

       for (var i = 0; i < files.length; i++) { 

           var name = files[i].replace('.png','');
           var classes = 'class="icon icon-' + name + '"' ;

           var span    = 
           '<span class="iconsnavigation__item">\n'+
             '<span '+ classes +'></span>\n'+
             '<span class="iconsnavigation__name"> .'+ name +'</span>\n'+
           '</span>\n';

           html += span;
       }
    
    html += '</div>'

    fs.outputFile('./help/nav_icons.html', html, function (err) {
        if (err) console.log(err);
    })
            
    });






});


/*
 * gulp land --env names : Создание html,css файлов по шаблону (сниппетам)
 * name: Можно передать 1 или более имен через запятую ','
 * example: gulp land --env header,footer,slider
*/
gulp.task('land', function() {

    if (!args.env)
        throw console.log('Ошибка: Не установленно имя блока')

    var blocks = args.env.split(',');
 
 
    for (var i = 0; i < blocks.length; i++) { 
 
        //created html files!
        var html = fs.readFileSync('help/snippets/land.block.html', {encoding: 'utf8'}, function (err, data) {
          if (err) throw err;
        });

        html = html.replace(/land/g, blocks[i]);
        var pathhtml = 'b/'+ blocks[i] +'.block.html'

        console.log(pathhtml)
        fs.outputFile(pathhtml, html,  function (err) {
          if (err) console.log(err);
          console.log("html file created")
        }); 


        //created css files!
        var css = fs.readFileSync('help/snippets/land.block.css', {encoding: 'utf8'}, function (err, data) {
          if (err) throw err;
        });

        css = css.replace(/land/g, blocks[i]);
        var pathcss = 'assets/css/'+ blocks[i] +'.block.css'

        console.log(pathcss)
        fs.outputFile(pathcss, css,  function (err) {
          if (err) console.log(err);
          console.log("css file created")
        });

    }
   
});

 