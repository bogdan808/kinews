;$(function() {
 
	$.get('/help/navigation.html', function(data) { 
		$('body').append(data); 

		$('.helpnavigation').hover(function() {
			$(this).toggleClass('show');
		}, function() {
			$(this).toggleClass('show');
		});

	});

	$.get('/help/nav_icons.html', function(data) { 
		$('.helpnavigation__icons').append(data); 
	});

	$.get('/help/nav_pages.html', function(data) { 

		$('.helpnavigation__pages').append(data); 

		$('.helpnavigation__item').hover(function() {

			$('.helpnavigation__content').hide();

			var content = $(this).data('content'); 
			$(content).show();

		}, function() {
			/* Stuff to do when the mouse leaves the element */
		});

	});
 




});